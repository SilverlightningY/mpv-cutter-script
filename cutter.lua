local function ensure_trailing_slash(path)
  if (path:sub(-1) == "/") then
    return path
  else
    return (path .. "/")
  end
end
local timelimits = {60, (60 * 60), (60 * 60 * 24)}
local function seconds__3etimestamp(seconds)
  local function add_leading_zero(number)
    if (number < 10) then
      return ("0" .. number)
    else
      return number
    end
  end
  local function get_highest_index_of_timelimits(seconds0)
    local index = 1
    while ((index <= #timelimits) and (seconds0 >= timelimits[index])) do
      index = (index + 1)
    end
    return (index - 1)
  end
  local function construct_time_format(seconds0)
    local time_string = ""
    local rem = seconds0
    for i = get_highest_index_of_timelimits(seconds0), 0, -1 do
      if (i == 0) then
        time_string = (time_string .. add_leading_zero(rem))
      else
        local time_value = math.floor((rem / timelimits[i]))
        time_string = (time_string .. add_leading_zero(time_value) .. ":")
        rem = (rem % timelimits[i])
      end
    end
    return time_string
  end
  local parsed_seconds = tonumber(seconds)
  if (seconds == nil) then
    return ("can't convert " .. seconds .. " to number")
  else
    return construct_time_format(parsed_seconds)
  end
end
local function to_lua_pattern(str)
  local escape_function
  local function _5_(special_char)
    return ("%" .. special_char)
  end
  escape_function = _5_
  local special_chars_removed, _ = str:gsub("[%(%)%.%%%+%-%*%?%[%^%$]", escape_function)
  local spaces_removed, _0 = special_chars_removed:gsub("%s", "%%s")
  return spaces_removed
end
local function get_initial_fragment_number(configuration, mp_utils)
  local max_fragment_number = 0
  if configuration["output-directory-present?"]() then
    local output_directory = configuration["user-options"].output_directory
    local files = mp_utils.readdir(output_directory, "files")
    local output_filename = mp.get_property("filename/no-ext")
    local filename_pattern = (to_lua_pattern(output_filename) .. "%s%((%d+)%).*")
    for _, file in ipairs(files) do
      local _0, name = mp_utils.split_path(file)
      local matches = string.gmatch(name, filename_pattern)
      for number in matches do
        local number0 = tonumber(number)
        if (number0 >= max_fragment_number) then
          max_fragment_number = number0
        else
        end
      end
    end
  else
  end
  return max_fragment_number
end
local function construct_notification_handler(mp, mp_msg, level)
  local function show_text_osd(text)
    return mp.commandv("show-text", text)
  end
  local function show_text_cli(text, l)
    return mp_msg[(l or level)](text)
  end
  local function show_text_common(text, l)
    show_text_osd(text)
    return show_text_cli(text, l)
  end
  local function show_cut_counter(current, total)
    return show_text_osd(("(" .. current .. "/" .. total .. ")"))
  end
  local function show_position_keyframe_set(position, seconds, l)
    local display_text = ("set " .. position .. " keyframe at: ")
    show_text_cli((display_text .. seconds), l)
    return show_text_osd((display_text .. seconds__3etimestamp(seconds)))
  end
  local function show_position_keyframe_deleted(position, l)
    return show_text_common((position .. " keyframe deleted"), l)
  end
  local function fragment_to_string(fragment)
    return ("{ start: " .. fragment.start .. ", end: " .. fragment["end"] .. " }")
  end
  local function show_fragments(fragments)
    if (#fragments > 0) then
      local string_representation
      do
        local string = ""
        local function _8_()
          local tbl_17_auto = {}
          local i_18_auto = #tbl_17_auto
          for _, fragment in ipairs(fragments) do
            local val_19_auto = fragment_to_string(fragment)
            if (nil ~= val_19_auto) then
              i_18_auto = (i_18_auto + 1)
              do end (tbl_17_auto)[i_18_auto] = val_19_auto
            else
            end
          end
          return tbl_17_auto
        end
        for i, fragment in ipairs(_8_()) do
          if (i == #fragments) then
            string = (string .. "\9" .. fragment)
          else
            string = (string .. "\9" .. fragment .. ",\n")
          end
        end
        string_representation = string
      end
      return show_text_cli(("Fragments {\n" .. string_representation .. "\n}"))
    else
      return show_text_cli("Fragments {}")
    end
  end
  local function show_cutting_started(l)
    return show_text_common("cutting started...", l)
  end
  local function show_model_nil_error()
    return show_text_cli("The script is unusable because the model is nil", "error")
  end
  local function show_fragment_scheduled(fragment, current)
    return show_text_cli((fragment_to_string(fragment) .. " (" .. current .. ") ...scheduled"))
  end
  local function show_is_cut(current, total, fragment, path)
    show_cut_counter(current, total)
    return show_text_cli((fragment_to_string(fragment) .. " -> " .. path .. " ...saved"))
  end
  return {["show-cut-counter"] = show_cut_counter, ["show-cutting-started"] = show_cutting_started, ["show-fragment-scheduled"] = show_fragment_scheduled, ["show-fragments"] = show_fragments, ["show-is-cut"] = show_is_cut, ["show-model-nil-error"] = show_model_nil_error, ["show-position-keyframe-deleted"] = show_position_keyframe_deleted, ["show-position-keyframe-set"] = show_position_keyframe_set, ["show-text-cli"] = show_text_cli, ["show-text-common"] = show_text_common, ["show-text-osd"] = show_text_osd}
end
local function read_user_options(mp_utils, mp_options)
  local options = {output_directory = mp_utils.getcwd(), output_file_extension = "mkv"}
  mp_options.read_options(options, "cutter")
  do end (options)["output_directory"] = ensure_trailing_slash(options.output_directory)
  do end (options)["keep-open"] = mp.get_property("keep-open")
  return options
end
local function construct_configuration(mp_utils, mp_options)
  local user_options = read_user_options(mp_utils, mp_options)
  local function output_directory_present_3f()
    local info = mp_utils.file_info(user_options.output_directory)
    return (info and info.is_dir)
  end
  return {["output-directory-present?"] = output_directory_present_3f, ["user-options"] = user_options}
end
local function construct_cutter(mp, mp_utils, notification_handler, configuration, model)
  local model0 = model
  local configuration0 = configuration
  local function get_configuration()
    return configuration0
  end
  local function set_configuration(c)
    configuration0 = c
    return nil
  end
  local function get_model()
    return model0
  end
  local function set_model(m)
    model0 = m
    return nil
  end
  local function check_model(on_success)
    local function _12_(...)
      if model0 then
        return on_success(...)
      else
        return notification_handler["show-model-nil-error"]()
      end
    end
    return _12_
  end
  local function set_keyframe()
    local timestamp = mp.get_property("time-pos")
    local keep_open = mp.get_property("keep-open")
    if (keep_open == "no") then
      mp.set_property("keep-open", "yes")
    else
    end
    if (model0)["start-keyframe"] then
      table.insert((model0).fragments, {start = (model0)["start-keyframe"], ["end"] = timestamp})
      do end (model0)["start-keyframe"] = nil
      return notification_handler["show-position-keyframe-set"]("stop", timestamp)
    else
      model0["start-keyframe"] = timestamp
      return notification_handler["show-position-keyframe-set"]("start", timestamp)
    end
  end
  local function delete_keyframe()
    if (model0)["start-keyframe"] then
      model0["start-keyframe"] = nil
      return notification_handler["show-position-keyframe-deleted"]("start")
    else
      if (#(model0).fragments > 0) then
        local fragments = (model0).fragments
        model0["start-keyframe"] = fragments[#fragments].start
        table.remove((model0).fragments)
        notification_handler["show-position-keyframe-deleted"]("stop")
        if (#fragments == 0) then
          return mp.set_property("keep-open", (configuration0)["user-options"]["keep-open"])
        else
          return nil
        end
      else
        return nil
      end
    end
  end
  local function show_fragments()
    return notification_handler["show-fragments"]((model0).fragments)
  end
  local function on_success(callback)
    local function async_handler(successful_3f, result, err)
      if successful_3f then
        return callback(result)
      else
        return notification_handler["show-text-cli"](err, "error")
      end
    end
    return async_handler
  end
  local function concat_fragment_path(index)
    local user_options = (configuration0)["user-options"]
    return (user_options.output_directory .. mp.get_property("filename/no-ext") .. " (" .. index .. ")." .. user_options.output_file_extension)
  end
  local function create_directory(path)
    return mp.command_native({name = "subprocess", args = {"mkdir", path}})
  end
  local function cut()
    notification_handler["show-cutting-started"]()
    if not configuration0["output-directory-present?"]() then
      create_directory((configuration0)["user-options"].output_directory)
    else
    end
    local fragment_length = #(model0).fragments
    local fragment_number = get_initial_fragment_number(configuration0, mp_utils)
    local cut_counter = 0
    for index, fragment in ipairs((model0).fragments) do
      local fragment_number0 = (index + fragment_number)
      local fragment_path = concat_fragment_path(fragment_number0)
      local cmd_args = {"ffmpeg", "-i", mp.get_property("path"), "-ss", fragment.start, "-to", fragment["end"], "-c", "copy", fragment_path}
      local native_command_table = {name = "subprocess", args = cmd_args, capture_stdout = "yes", capture_stderr = "yes", detach = "yes", playback_only = "no"}
      local on_result
      local function _21_(result)
        cut_counter = (cut_counter + 1)
        return notification_handler["show-is-cut"](cut_counter, fragment_length, fragment, fragment_path)
      end
      on_result = on_success(_21_)
      notification_handler["show-fragment-scheduled"](fragment, index)
      mp.command_native_async(native_command_table, on_result)
    end
    model0["fragments"] = {}
    return nil
  end
  return {["set-keyframe"] = check_model(set_keyframe), ["delete-keyframe"] = check_model(delete_keyframe), ["show-fragments"] = check_model(show_fragments), cut = check_model(cut), ["set-model"] = set_model, ["get-model"] = get_model, ["set-configuration"] = set_configuration, ["get-configuration"] = get_configuration}
end
local function construct_cutter_model()
  return {["start-keyframe"] = nil, fragments = {}}
end
local mp = require("mp")
local mp_msg = mp.msg
local mp_options = require("mp.options")
local mp_utils = require("mp.utils")
local notification_handler = construct_notification_handler(mp, mp_msg, "info")
local cutter = construct_cutter(mp, mp_utils, notification_handler)
local configuration = construct_configuration(mp_utils, mp_options)
cutter["set-configuration"](configuration)
local function on_start_file()
  return cutter["set-model"](construct_cutter_model())
end
local function on_reset()
  if not (mp.get_property("keep-open") == configuration["user-options"]["keep-open"]) then
    mp.set_property("keep-open", configuration["user-options"]["keep-open"])
  else
  end
  cutter["set-model"](construct_cutter_model())
  return notification_handler["show-text-cli"]("reset")
end
local function show_model()
  return notification_handler["show-text-cli"](("model " .. mp_utils.to_string(cutter["get-model"]())))
end
mp.register_event("start-file", on_start_file)
mp.add_key_binding("Ctrl+Shift+r", on_reset)
mp.add_key_binding("Ctrl+Shift+m", show_model)
mp.add_key_binding("Ctrl+l", cutter["show-fragments"])
mp.add_key_binding("Ctrl+k", cutter["set-keyframe"])
mp.add_key_binding("Ctrl+r", cutter["delete-keyframe"])
return mp.add_key_binding("Ctrl+Shift+c", cutter.cut)
