SRC := $(wildcard *.fnl)
OUT := $(patsubst %.fnl,%.lua,$(SRC))

all: compile

%.lua: %.fnl
	fennel --compile $< > $@

compile: $(OUT)

clean:
	rm $(OUT)
