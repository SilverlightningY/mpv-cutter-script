require "mp"
require "mp.msg"
require "mp.options"
utils = require "mp.utils"

-- util functions

function check_directory(dir)
    info = utils.file_info(dir)
    if(info and info.is_dir)
    then
        mp.msg.info("output directory: " ..dir)
    else
        mp.msg.error(dir .. " is not a valid directory")
    end
end

function probably_append_trailing_slash(path)
    if (not path:sub(-1) == "/") then
        return path .. "/"
    end
    return path
end

function table_length(table)
    local count = 0
    for _ in pairs(table) do count = count + 1 end
    return count
end

-- init options and meta data

local options = {
    output_folder = utils.getcwd(),
    output_file_extension = "mkv",
    normalize_audio = false
}
read_options(options, "cutter")
options.output_folder = probably_append_trailing_slash(options.output_folder)
check_directory(options.output_folder)

local Fragment = {}

function Fragment:new(o, start, stop)
    o = o or {}
    setmetatable(o, self)
    o.__index = self
    o.start = start
    o.stop = stop
    return o
end

local continuation_counter = 1
local cut_counter = 0
local concat_fragments = {}
local concat_buffer = nil

function increase_cut_counter()
    cut_counter = cut_counter + 1
    mp.command("show-text \"(" .. cut_counter .. "/" .. table_length(concat_fragments) .. ")\"")
end

function get_async_handler(fn)
    return function(success, result, error)
        if(success)
        then
            fn(result)
        else
            value = error
            mp.msg.error(utils.to_string(error))
        end
    end
end

-- interop with fragments

function set_concat_keyframe()
    local curr_time = mp.get_property("time-pos")
    if (not concat_buffer)
    then
        mp.msg.info("start keyframe at: ", curr_time)
    	mp.command("show-text \"start keyframe at ${time-pos}\"")
        concat_buffer = curr_time
    else
        mp.msg.info("stop keyframe at: ", curr_time)
    	mp.command("show-text \"stop keyframe at ${time-pos}\"")
        local fragment = Fragment:new(nil, concat_buffer, curr_time)
        table.insert(concat_fragments, fragment)
        concat_buffer = nil
    end
end

function remove_concat_keyframe()
    if (concat_buffer)
    then
        concat_buffer = nil
        print("start keyframe deleted")
	mp.commandv("show-text", "start keyframe deleted")
    else
        table.remove(concat_fragments, table.maxn(concat_fragments))
        print("last fragment deleted")
	mp.commandv("show-text", "last Fragment deleted")
    end
end

function print_table()
    for k,v in ipairs(concat_fragments)
    do
        print("\t{start: \"" .. v.start .. "\", stop: \"" .. v.stop .. "\"}")
    end
    -- mp.msg.info(utils.to_string(concat_fragments))
end

-- cut util functions

function cut_into_fragments()
    mp.msg.info("cutter started...")
    mp.commandv("show-text", "cutter started...")
    if(options.normalize_audio)
    then
        for k,v in ipairs(concat_fragments)
        do
            get_max_volume_of_fragment_async(v, 
                function(fragment, volume)
                    volume = invert_volume(volume)
                    local filename = create_filename()
                    cut_async(fragment, volume, filename)
                end
            )
        end
    else
        for k,fragment in ipairs(concat_fragments)
        do
            local filename = create_filename()
            cut_async(fragment, nil, filename)
        end
    end
end

function get_max_volume_of_fragment_async(fragment, callback)
    local handler = get_async_handler(
        function(result)
            local match = string.match(result.stderr, "max_volume:%s+(%-?%d+%.%d+)")
            callback(fragment, match)
        end
    )
    print("detect volume from ".. fragment.start .. " to " .. fragment.stop)
    return mp.command_native_async({
        name="subprocess", 
        args={"ffmpeg", "-i", mp.get_property('path'), "-ss", fragment.start, "-to", fragment.stop, "-filter:a", "volumedetect", "-f", "null", "/dev/null"},
        capture_stdout="yes", 
        capture_stderr="yes"}, handler)
end


function invert_volume(volume)
    if(string.sub(volume, 1, 1) == "-")
    then
        return string.sub(volume, 2)
    else
        return "-"..volume
    end
end

function create_filename()
    local filename = mp.get_property("filename/no-ext")
    filename = options.output_folder .. "/" .. filename .." (" .. continuation_counter .. ")." .. options.output_file_extension
    continuation_counter = continuation_counter + 1
    return filename
end

function cut_async(fragment, volume, filename)
    local handler = get_async_handler(
        function(result)
            mp.msg.info(filename .. " is cut")
	    increase_cut_counter()
        end
    )
    local cmd_args = nil
    if(volume)
    then
        cmd_args = {"ffmpeg", "-i", mp.get_property("path"), "-ss", fragment.start, "-to", fragment.stop, "-c:v", "copy", "-filter:a", "volume="..volume.."dB", filename}
    else
        cmd_args = {"ffmpeg", "-i", mp.get_property("path"), "-ss", fragment.start, "-to", fragment.stop, "-c", "copy", filename}
    end
    print("cut "..filename)
    return mp.command_native_async({
        name="subprocess",
        args=cmd_args,
        capture_stdout="yes",
        capture_stderr="yes"}, handler)
end

-- event observer functions

function reset_continuation_counter()
    continuation_counter = 1
end

function probably_cut()
    if(table_length(concat_fragments) > 0)
    then
        cut_into_fragments()
    end
end

-- bindings

mp.add_key_binding("Ctrl+k", set_concat_keyframe)
mp.add_key_binding("Ctrl+r", remove_concat_keyframe)
mp.add_key_binding("Ctrl+l", print_table)
mp.add_key_binding("Ctrl+Shift+c", cut_into_fragments)

mp.register_event("start-file", reset_continuation_counter)
-- mp.add_hook("on_unload", 50, probably_cut)
