; Adds a trailing slash if missing.
(fn ensure-trailing-slash [path]
  "ensures that the string ends with a slash"
  (if (= (path:sub -1) "/")
    path
    (.. path "/")))

(local timelimits [60 (* 60 60) (* 60 60 24)])

(fn seconds->timestamp [seconds]
  "converts seconds to a time string in the format [dd:[hh:[mm:]]]ss.sss"
  (fn add-leading-zero [number]
    (if (< number 10)
      (.. "0" number)
      number))
  (fn get-highest-index-of-timelimits [seconds]
    (var index 1)
    (while (and (<= index (length timelimits)) (>= seconds (. timelimits index)))
      (set index (+ index 1)))
    (- index 1))
  (fn construct-time-format [seconds]
    (var time-string "")
    (var rem seconds)
    (for [i (get-highest-index-of-timelimits seconds) 0 -1]
      (if (= i 0)
        (set time-string (.. time-string (add-leading-zero rem)))
        (let [time-value (math.floor (/ rem (. timelimits i)))]
          (set time-string (.. time-string (add-leading-zero time-value) ":"))
          (set rem (% rem (. timelimits i))))))
    time-string)
  (let [parsed-seconds (tonumber seconds)]
    (if (= seconds nil)
      (.. "can't convert " seconds " to number")
      (construct-time-format parsed-seconds))))

; Transforms a string into a lua pattern by escaping special characters.
(fn to-lua-pattern [str]
  (let [escape-function (fn [special-char] (.. "%" special-char))
        (special-chars-removed _) (str:gsub "[%(%)%.%%%+%-%*%?%[%^%$]" escape-function)
        (spaces-removed _) (special-chars-removed:gsub "%s" "%%s")]
    spaces-removed))

; Extracts the highest suffix number if a file in the destination directory
; exists with the same name as the current. E. g. dir/abc (3).mkv returns 3.
(fn get-initial-fragment-number [configuration mp-utils]
  (var max-fragment-number 0)
  (if (configuration.output-directory-present?)
    (let [output-directory (. configuration :user-options :output_directory)
          files (mp-utils.readdir output_directory "files")
          output-filename (mp.get_property "filename/no-ext")
          filename-pattern (.. (to-lua-pattern output-filename) "%s%((%d+)%).*")]
      (each [_ file (ipairs files)]
        (let [(_ name) (mp-utils.split_path file)
              matches (string.gmatch name filename-pattern)]
          (each [number matches]
            (let [number (tonumber number)]
              (if (>= number max-fragment-number)
                (set max-fragment-number number))))))))
  max-fragment-number)

; Constructs a notification handler which is used for logging to console and
; printing to the OSD.
(fn construct-notification-handler [mp mp-msg level]
  (fn show-text-osd [text]
    (mp.commandv "show-text" text))
  (fn show-text-cli [text l]
    ((. mp-msg (or l level)) text))
  (fn show-text-common [text l]
    (show-text-osd text)
    (show-text-cli text l))
  (fn show-cut-counter [current total]
    (show-text-osd (.. "(" current "/" total ")")))
  (fn show-position-keyframe-set [position seconds l]
    (let [display-text (.. "set " position " keyframe at: ")]
      (show-text-cli (.. display-text seconds) l)
      (show-text-osd (.. display-text (seconds->timestamp seconds)))))
  (fn show-position-keyframe-deleted [position l]
    (show-text-common (.. position " keyframe deleted") l))
  (fn fragment-to-string [fragment]
    (.. "{ start: " (. fragment :start) ", end: " (. fragment :end) " }"))
  (fn show-fragments [fragments]
    (if (> (length fragments) 0)
      (let [string-representation (accumulate [string ""
                                               i fragment (ipairs (icollect [_ fragment (ipairs fragments)]
                                                                            (fragment-to-string fragment)))]
                                              (if (= i (length fragments))
                                                (.. string "\t" fragment)
                                                (.. string "\t" fragment ",\n")))]
        (show-text-cli (.. "Fragments {\n" string-representation "\n}")))
                         
      (show-text-cli "Fragments {}")))
  (fn show-cutting-started [l]
    (show-text-common "cutting started..." l))
  (fn show-model-nil-error []
    (show-text-cli "The script is unusable because the model is nil" :error))
  (fn show-fragment-scheduled [fragment current]
    (show-text-cli (.. (fragment-to-string fragment) " (" current ") ...scheduled")))
  (fn show-is-cut [current total fragment path]
    (show-cut-counter current total)
    (show-text-cli (.. (fragment-to-string fragment) " -> " path " ...saved")))
  {: show-cut-counter
   : show-cutting-started
   : show-fragment-scheduled
   : show-fragments
   : show-is-cut
   : show-model-nil-error
   : show-position-keyframe-deleted
   : show-position-keyframe-set
   : show-text-cli
   : show-text-common
   : show-text-osd})
   
; Reads the user-options and applies them to the default configuration.
(fn read-user-options [mp-utils mp-options]
  (let [options {"output_directory" (mp-utils.getcwd)
                 "output_file_extension" "mkv"}]
    (mp-options.read_options options "cutter")
    (tset options :output_directory (ensure-trailing-slash (. options :output_directory)))
    (tset options :keep-open (mp.get_property :keep-open))
    options))

; Constructs a configuration object which contains functions that are helpfull
; for managing the configuration.
(fn construct-configuration [mp-utils mp-options] 
  (let [user-options (read-user-options mp-utils mp-options)]
    (fn output-directory-present? []
      (let [info (mp-utils.file_info (. user-options "output_directory"))]
        (and info info.is_dir)))
    {: output-directory-present? 
     : user-options}))

; Construct a cutter which is cappable of splitting the current file into
; smaller parts.
(fn construct-cutter [mp mp-utils notification-handler configuration model]
  (var model model)
  (var configuration configuration)
  (fn get-configuration []
    configuration)
  (fn set-configuration [c]
    (set configuration c))
  (fn get-model []
    model)
  (fn set-model [m]
    (set model m))
  ; checks if model is set
  (fn check-model [on-success]
    (fn [...]
      (if model
        (on-success ...)
        (notification-handler.show-model-nil-error))))
  ; sets a keyframe at the current time-pos
  (fn set-keyframe []
    (let [timestamp (mp.get_property "time-pos") keep-open (mp.get_property :keep-open)]
      (if (= keep-open :no)
        (mp.set_property :keep-open :yes))
      (if (. model :start-keyframe)
        (do
          (table.insert (. model :fragments) {:start (. model :start-keyframe) :end timestamp})
          (tset model :start-keyframe nil)
          (notification-handler.show-position-keyframe-set :stop timestamp))
        (do
          (tset model :start-keyframe timestamp)
          (notification-handler.show-position-keyframe-set :start timestamp)))))
  ; deletes the latest start keyframe or fragment
  (fn delete-keyframe []
    (if (. model :start-keyframe)
      (do
        (tset model :start-keyframe nil)
        (notification-handler.show-position-keyframe-deleted :start))
      (if (> (length (. model :fragments)) 0)
        (let [fragments (. model :fragments)]
          (tset model :start-keyframe (. fragments (length fragments) :start))
          (table.remove (. model :fragments))
          (notification-handler.show-position-keyframe-deleted :stop)
          (if (= (length fragments) 0)
            (mp.set_property :keep-open (. configuration :user-options :keep-open)))))))
  ; shows all fragments currently in memory
  (fn show-fragments []
    (notification-handler.show-fragments (. model :fragments)))
  (fn on-success [callback]
    (fn async-handler [successful? result err]
      (if successful? 
        (callback result)
        (notification-handler.show-text-cli err :error))))
  (fn concat-fragment-path [index]
    (let [user-options (. configuration :user-options)]
      (.. 
        (. user-options :output_directory) 
        (mp.get_property "filename/no-ext") 
        " (" index ")." (. user-options :output_file_extension))))
  (fn create-directory [path]
    (mp.command_native {:name "subprocess"
                        :args ["mkdir" path]}))
  ; cuts the video into parts
  (fn cut []
    (notification-handler.show-cutting-started)
    (if (not (configuration.output-directory-present?))
      (create-directory (. configuration :user-options :output_directory)))
    (let [fragment-length (length (. model :fragments))
          fragment-number (get-initial-fragment-number configuration mp-utils)]
      (var cut-counter 0)
      (each [index fragment (ipairs (. model :fragments))]
        (let [fragment-number (+ index fragment-number)
              fragment-path (concat-fragment-path fragment-number)
              cmd-args ["ffmpeg" 
                        "-i" (mp.get_property "path") 
                        "-ss" (. fragment :start) 
                        "-to" (. fragment :end) 
                        "-c" "copy" 
                        fragment-path]
              native-command-table {:name "subprocess" 
                                    :args cmd-args 
                                    :capture_stdout "yes" 
                                    :capture_stderr "yes"
                                    :detach "yes"
                                    :playback_only "no"}
              on-result (on-success (fn [result]
                                      (set cut-counter (+ cut-counter 1))
                                      (notification-handler.show-is-cut 
                                        cut-counter fragment-length fragment fragment-path)))]
          (notification-handler.show-fragment-scheduled fragment index)
          (mp.command_native_async native-command-table on-result)))
      (tset model :fragments {})))
  {:set-keyframe (check-model set-keyframe)
   :delete-keyframe (check-model delete-keyframe)
   :show-fragments (check-model show-fragments)
   :cut (check-model cut)
   : set-model
   : get-model
   : set-configuration
   : get-configuration})

; Constructs a cutter-model which holds the internal state of a cutter.
(fn construct-cutter-model []
  {:start-keyframe nil
   :fragments {}})

(let [mp (require :mp) 
      mp-msg (. mp :msg)
      mp-options (require :mp.options)
      mp-utils (require :mp.utils)
      notification-handler (construct-notification-handler mp mp-msg :info)
      cutter (construct-cutter mp mp-utils notification-handler)
      configuration (construct-configuration mp-utils mp-options)]
  (cutter.set-configuration configuration)

  (fn on-start-file []
    (cutter.set-model (construct-cutter-model)))

  (fn on-reset []
    (if (not (= (mp.get_property :keep-open) (. configuration :user-options :keep-open)))
      (mp.set_property :keep-open (. configuration :user-options :keep-open)))
    (cutter.set-model (construct-cutter-model))
    (notification-handler.show-text-cli "reset"))

  (fn show-model []
    (notification-handler.show-text-cli (.. "model " (mp-utils.to_string (cutter.get-model)))))

  (mp.register_event "start-file" on-start-file)
  (mp.add_key_binding "Ctrl+Shift+r" on-reset)
  (mp.add_key_binding "Ctrl+Shift+m" show-model)
  (mp.add_key_binding "Ctrl+l" (. cutter :show-fragments))
  (mp.add_key_binding "Ctrl+k" (. cutter :set-keyframe))
  (mp.add_key_binding "Ctrl+r" (. cutter :delete-keyframe))
  (mp.add_key_binding "Ctrl+Shift+c" (. cutter :cut)))
