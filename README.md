# mpv cutter script
## installation
copy the `cutter.lua` script to `XDG_CONFIG_HOME/mpv/scripts` or `~\AppData\Roaming\mpv\scripts`

## configuration
The following options can be tweaked in `MPV_HOME/script-opts/cutter.conf`:
|option|type|value|description|
|-|-|-|-|
|`output_directory`|string|Path (absolute/relative)|Directory to save the fragments in|
|`output_file_extension`|string|File extension supported by `ffmpeg`|File extension for each cutted fragment|
### Example
```config
output_directory=./temp
output_file_extension=mkv
```
The options can be passed to mpv via cli arguments by prefixing them with `--script-opts=cutter-<option>`

## usage
|key|action|
|-|-|
|`Ctrl+k`|Sets a start or stop keyframe at the current playback position (timestamp)|
|`Ctrl+r`|Removes last keyframe|
|`Ctrl+l`|Prints the timestamp table to console|
|`Ctrl+Shift+r`|Resets all changed values|
|`Ctrl+Shift+c`|Starts the cut process|
### Info
- The cutting is done asynchronous so the order of the cutted segments
  displayed in console may be shuffled but the outputed segments are still in
  the right order.
- Each outputed fragment gets suffixed by ` (#)`. To avoid name clashes no file
  with this scheme should exist in output directory. The script will not
  override any files.
